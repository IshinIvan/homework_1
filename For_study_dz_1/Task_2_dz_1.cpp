/*�������� ���������, ������� ��������� ������� �������������� ��������� ������� 
��� ����� ������������ � ������������� ��������� �������. 
���� �������� ������������� ��� ������ �� ����� ������ ��������� 
(������, ��������� �������������, �������� ���������� �������).
������� �������������� ��� ����� min � max ��������. 
������� ������ (10 ����� ����� � ����� ������) ->12 10 5 7 15 4 10 17 23 7 
����������� �������: 4 ������������ �������: 23 ������� �����. ��� ����� min � max ��������: 10.36*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

char find_minmax(int *a, int k)
{
	int min = a[0];
	int max = a[0];
	int qwe = 0;
	int count_qwe = 0;
	for (int i = 1; i < k; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
		if (a[i] < min)
		{
			min = a[i];
		}
	}
	for (int i = 0; i < k; i++)
	{
		if ((a[i] != min) & (a[i] != max))
		{
			qwe += a[i];
			count_qwe++;
		}
	}
	cout << endl << "min = " << min;
	cout << endl << "max = " << max;
	double sum = double(qwe) / double(count_qwe);
	cout << endl << "Srenie arifmeticheskoe elementov massiva bez min & max = " << setprecision(2) << sum;
	return ' ';
}

int main()
{
	srand(time(NULL));
	int count;
	cout << "Vvedite kol-vo elementov massiva: count= ";
	cin >> count;
	int *array = new int[count];
	cout << endl << "Elementi massiva s minmax   -> ";
	for (int i = 0; i < count; i++)
	{
		array[i] = rand() % 10;;
		cout << array[i] << '\t';
	}
	
	cout << endl << find_minmax(array, count) << endl;
	delete[] array;
	return 0;
}