/*������� �������� ������� � ������� � ���������� ��������� ���������. ����������� ��������� ��������:
1. ��������� ������ int a[10] ���������� �������
2. � ���������� max ��������� ������� ������� a[0], � � ���������� index � 0.
3. ������� � ���������� �������� �������
4. ���� ������� ������� ������ �������� ���������� max, 
	�� ��������� ��� �������� � ���������� max, � ��� ������ � ���������� index
5. ���� ������� ������� � �� ���������, �� ������� � ������ 2
6. ������� ������ a � �������� ���������� max � index
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	int array[10];
	srand(time(NULL));
	int index = 0;
	cout << "Array -> ";
	for (int i = 0; i < 10; i++)
	{
		array[i] = rand() % 100;
		cout << array[i] << '\t';
	}
	int max = array[0];
	for (int i = 1; i < 10; i++)
	{
		if (array[i] > max)
		{
			max = array[i];
			index = i;
		}
	}
	cout << endl << "Max = " << max;
	cout << endl << "Number of max (if the first element 0) = " << index << endl;
	cout << "Number of max (if the first element 1) = " << (index+1) << endl;
	return 0;
}