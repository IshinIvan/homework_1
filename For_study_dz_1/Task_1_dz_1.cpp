/*�������� ��� ������������� ������� � ������� ��������� � �������� �������,
������� ��������� �� �������� ���������� � ���������� �� �����.
������� ������ ��������� ��� ��������� � ������ � ��� ������*/
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

char Zapolnenie(int *a, int count)
{
	for (int i = 0; i < count; i++)
	{
		a[i] = rand() % 100;
		cout << a[i] << '\t';
	}
	return ' ';
}

int main()
{
	int count_1, count_2;
	cout << "Vvedite kol-vo elementov 1ogo massiva: count_1= ";
	cin >> count_1;
	cout << endl << "Vvedite kol-vo elementov 2ogo massiva: count_2= ";
	cin >> count_2;
	srand(time(NULL));
	int *array_1 = new int[count_1];
	int *array_2 = new int[count_2];
	cout << endl << "Elementi 1ogo massiva: ";
	cout << endl << Zapolnenie(array_1, count_1) << endl;
	cout << "Elementi 2ogo massiva: ";
	cout << Zapolnenie(array_2, count_2) << endl;
	delete[] array_1;
	delete[] array_2;
	return 0;
}